#!/usr/bin/env python3
# POC: 
#Author: scp
#import random #always, always import random...
import csv
import datetime
from honeyPigSprocs import *
import os
import random
import re
#import scapy
#from scapy.all import *
from scapy.all import rdpcap
import socket
import sqlite3#https://www.sqlite.org/inmemorydb.html
import subprocess
import sys
import time
from turtle import Turtle,Screen
#import whois

cwd=os.getcwd()
c=(0,0,0)

class pig:#gerneric data flow object, POOP! #honeyPig specific attribs 
  def __init__(self, name='napolion'+str(datetime.datetime.now()), db = cwd+ '/data/Bastards.db copy'):
    self.cwd = cwd
    self.name = name
    self.data_cli = []#data for use or thread
    self.ds = []#datasets from SQL
    self.db = db#dB for SQL
    self.pcap = "./data/2020-01-29-Qbot-infection-traffic.pcap"#Bastards.pcap copy"#default pcap
    self.mens_rea = False#script is invasive or malicious?
    self.sql = getSproc(0,)#load default stored procedure
    self.ts=datetime.datetime.now()#this pig's birth date
    self.bbq()

  def bbq(self):print('let\'s eat ' + self.name,"cook time:",str(datetime.datetime.now()-self.ts))

  def feed(self):#load ds from db's, make lists n' stuff 
    dbLand(self) 
    for i in self.ds:self.data_cli.append(i[0])

  def shovel(self):#print current queue
    for i in self.data_cli:print(i)#show data in queue
#Util===================================================  
def dbLand(some_pig):#ANY db conn
  #sqlite import sqlite3 #https://www.sqlite.org/inmemorydb.html
  #if sqlite:
    conn = sqlite3.connect(some_pig.db)#You can also supply the special name :memory: to create a database in RAM.
    c = conn.cursor()
    c.execute(some_pig.sql)
    some_pig.ds = c.fetchall()
    conn.commit()
    conn.close()
  #elif mongo:
  #else: your DB conn

def readPcap(some_pig,crap = []):#must have scapy!
    for p in rdpcap(some_pig.pcap):#load pcap [telemetry] to pig object 
      if p[0].haslayer('IP'):crap.append(p[0]['IP'].src)#RHOST int(p[0].time p.show()
    for i in list(set(crap)):some_pig.data_cli.append(i)#Unique to plt faster unless chronology needed

def readCSV(some_pig,f="./data/bogonAWSIPs.csv"):
  with open(f, newline='') as csvfile:
    web=csv.reader(csvfile, delimiter=',', quotechar='|')
    for charlotte in web:
      for i in charlotte: some_pig.data_cli.append(i)

#Turtle=================================================
def jam(t,r,c,b,f,l=True,n=90):#turtle, rotation, octet, plot (pen Up/Down), line length
  t.setheading(n)#90==North, Up
  if l:t.left(r*(c))#counter clockwise
  else: t.right(r*(c))#clockwise...mirrors...
  if b:t.up()
  t.forward(f)
  if b:t.down()

def moves(p):
  t=spawn()
  dataLen=len(p.data_cli)
  big=dataLen>3000
  cnt=0
  thermo=-1
  deadPool=[]
  print("rec count:",dataLen)
  for ip in p.data_cli:
    dp=ip[0:ip.rfind(".")]
    cnt=cnt+1
    t.octs=[]
    for v in ip.split('.'):t.octs.append(int(v))#octs = ip.split('.')
    c=(int(t.octs[0]),int(t.octs[1]),int(t.octs[2]))
    t.color(c)
    pov=[t.pos()]#more than one center for loop, not always startpos
    for s in range(t.legs):
      show=(big and s<1)
      if s==3:
        povT=dp,t.pos()
        deadPool=list(set(deadPool))
        if dp not in deadPool:
          deadPool.append(dp)
          pov.append(povT)
      if dp in list(set(deadPool)):
        s=3
        nodeStart=[k[1] for k in pov if k[0]==dp]
        for k in nodeStart:
          x=k[0]
          y=k[1]
        ninja(t,(x,y))
      jam(t,t.rot[s][1],t.octs[s],show,t.fw[s+1])
    t.circle(1)
    pc=int((cnt/dataLen)*100)
    if not(thermo==pc):
      thermo=pc
      print(str(pc)+"% ",p.name,"...cook time:",str(datetime.datetime.now()-p.ts))
    if cnt/100==(cnt/100):t=spawn(t)  
    else:ninja(t,pov[0][0])

def spawn(t=None):#print(t.__dict__.keys()), 'startpos', 'legs', 'spoke', 'rot', 'fw', 'spokes', 'octs'
  del t#respawn reduced hyperbolic CPU turtle growth in big data pools
  return mutate(Turtle())

def ninja(t,pos):
  t.penup()
  t.goto(pos)
  t.pendown()
  
def printFinal(w,f = './data/ipFrac'+time.strftime("%Y%m%d-%H%M%S")+'.ps'):
  print(f)
  canvas = w.getcanvas()
  canvas.postscript(file=f)
  subprocess.call(['lpr', f])

def mutate(t):
  t.ht()#hide turtle
  t.speed(20)#fast as possible
  t.startpos=t.pos()#store plot paralax center
  t.speed(20)#setting the speed of the turtle
  t.pensize(1)
  t.setheading(90)#North, Up...
  t.legs=4
  t.spoke=[]
  t.rot=[]
  t.fw=[(0,700)]#Fit to window. Bigger is better until cropped. Zero idx!
  try: foo=t.spokes
  except: t.spokes=255#spread of graph, 255 is whole octet, lower to plot small range with greater spead & Vis
  for s in range(t.legs): t.spoke.append((s,t.spokes))
  for k,v in t.spoke: t.rot.append((k,360/v))#Degrees per spoke
  for k,v in t.spoke: t.fw.append(t.fw[0][1]/(2*(k+1)))#Fibonacci=pretty, change to focus spread by class
  return t

def setMW(w=Screen(),b=(15,15,15),t="<"+("="*20)+"Turtle walks IP; a Honey Pot Stream"+("="*20)+">"):#wn.setup(1200, 1024) #w.bgpic('bgimg1.png')
  w.setup(startx=None, starty=None)
  w.colormode(255)#octet visual to magic map...
  w.bgcolor(b)# compliment 240.0.0.0/4 240.0.0.0?255.255.255.254 268435455 InternetReserved for future use.[13] (Former Class E network.)
  w.title(t)#antisocial...change me
  return w

def mainPen(p,txt=False,db=False,sql=getSproc(),qry=False,pcap=False,cap=False,plotz=False):
  if txt:readCSV(p)#else: readCSV(mainPig,txt)
  if db:p.db=""
  p.sql=sql
  if qry:p.feed()#todo pandas datasets & mongo
  if pcap:p.pcap=pcap
  if cap:readPcap(p)#todo - set sniff
  if "show IP list":p.shovel()#todo - add output list to ./data/text.csv
  if plotz:moves(p)#todo - mirrors jam(t,r,c,b,f,l=False,n=90)
  
  
if __name__ == "__main__":
  mainPig = pig('babe'+str(datetime.datetime.now()))
  mainWindow=setMW()
  #for i in range(0,16):mainPen(mainPig,0,0,getSproc(False,i),1,0,0,1)#(p,text fn?,DB fn?,SQL txt?,run query,cap fn?,read pcap):
  mainPen(mainPig,0,0,getSproc(False,0),0,mainPig.pcap,1,1)#(p,text fn?,DB fn?,SQL txt?,run query,cap fn?,read pcap):
  mainPig.bbq()#mainWindow.bye() #wn.exitonclick()
  if True:printFinal(mainWindow)#printFinal(mainWindow,f)
