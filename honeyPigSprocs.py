#!/usr/bin/env python3
# POC: Stored Procedure-esque queries (good place for action qry's)
# qrys are for: honeyPig data model
#Author: scp
#import random #always, always import random...
def setDBMS(dbms="sqlite",act=False,param="AND 2=2 ",l=0):
    l="" if l==0 else " LIMIT "+l+";"
    w="WHERE 1=1 "+param
    if dbms=="sqlite":
        s="SELECT DISTINCT packet.clientIP FROM packet "
        t=" TEXT"
        B="/*" if not act else ""
        E="*/" if not act else ""
        vals="last_seen"+t+",srcIP"+t+",srcPort"+t+",clientIP"+t+",clientPort"+t+",data_cli"+t
        sproc=[s+w+"AND last_seen > DATE('now','-2 day') ORDER BY last_seen"
,s+l
,s+w+l
,s+w+"AND clientIP LIKE '192.168.%' or '10.%' or '127.%' or '0.%' or '255.%'"+l
,s+w+"AND clientIP LIKE '192.241.%'"+l
,s+"INNER JOIN packeteer ON packet.clientIP=packeteer.clientIP "+w+"AND hst LIKE '%onyphe.net%' OR packet.clientIP LIKE '51.255.%'"+l
,s+"LEFT JOIN packeteer ON packet.clientIP=packeteer.clientIP "+w+"AND hst LIKE '%onyphe.net%' ORDER BY packet.last_seen desc"+l
,"SELECT DISTINCT clientIP FROM (SELECT packet.clientIP, geo FROM packet LEFT JOIN packetgeo ON packet.clientIP = packetgeo.clientIP) "+w+"AND geo LIKE '%US,%' OR geo LIKE '%RU,%' OR geo LIKE '%CN,%'"+l
,s+"INNER JOIN packeteer ON packet.clientIP=packeteer.clientIP "+w+"AND data_cli LIKE 'YidHRVQ%'"+l
,s+"INNER JOIN packeteer ON packet.clientIP=packeteer.clientIP "+w+"AND data_cli LIKE '%bXN0c2hhc2g9%'"
,s+"INNER JOIN packeteer ON packet.clientIP=packeteer.clientIP "+w+"AND data_cli LIKE '%YidTU0gt%'"
,s+"INNER JOIN packeteer ON packet.clientIP=packeteer.clientIP "+w+"AND data_cli LIKE '%YidDTlhOXHgwMVx4MDBceDAwXHgwMVx4MDBceDAwXHgxMFx4MDBxXHgwMFx4MDBceDAw%'"
,s+"INNER JOIN packeteer ON packet.clientIP=packeteer.clientIP "+w+"AND data_cli LIKE '%YidHRVQgbG9naW4uY2dp%'"
,s+"INNER JOIN packeteer ON packet.clientIP=packeteer.clientIP "+w+"AND packeteer.domainName LIKE '%shodan%' OR packeteer.domainName LIKE '%censys%' OR packeteer.domainName LIKE '%binaryedge.ninja%'"
,s+"INNER JOIN packeteer ON packet.clientIP=packeteer.clientIP "+w+"AND packeteer.domainName LIKE '%shadowserver.org%' OR packeteer.domainName LIKE '%ampereinnotech.com%'"
,s+"INNER JOIN packeteer ON packet.clientIP=packeteer.clientIP "+w+"AND packeteer.domainName LIKE '%internet-census.org%'"
,"SELECT DISTINCT packet.clientIP FROM (WITH RECURSIVE octs(seq, D, IP,C,B, A,cIP) AS (SELECT DISTINCT 0, '.', packet.clientIP ||'.','','','',packet.clientIP FROM packet "+w+" UNION ALL SELECT seq+1,substr(IP,0,instr(IP,'.')),substr(IP,instr(IP,'.')+1),D,C,B,cIP FROM octs WHERE IP != '') SELECT A,B,C FROM octs WHERE seq=4 GROUP BY A*1,B*1,C*1 HAVING count(A)>8 ORDER BY count(a) DESC) INNER JOIN packet ON packet.clientIP LIKE A||'.'||B||'.'||C||'.%'"
,"SELECT '';","SELECT '';","SELECT '';","SELECT '20';","SELECT '21';","SELECT '22';","SELECT '23';","SELECT '24';","SELECT '25';"
,"SELECT DISTINCT clientIP FROM dialog "+w+l
,B+"DROP TABLE if exists packet;"+E
,B+"CREATE TABLE if not exists packet ("+vals+");"+E
,B+"INSERT INTO packet VALUES ("+vals+");"
,B+"SELECT '5';"+E,"SELECT '6';","SELECT '7';","SELECT '8';","SELECT '9';","SELECT '10';","SELECT '11';","SELECT '12';","SELECT '13';"
,B+"DROP TABLE if exists dialog;"+E
,B+"CREATE TABLE if not exists dialog ("+vals+");"+E
,B+"INSERT INTO dialog VALUES ("+vals+","+safeStr(param,'encode')+");"+E
,B+"SELECT * FROM dialog"+l+E
]
        return(sproc,param)
    else:return([],[],[])

def getSproc(act=False,idx=0,param="",dbms="sqlite"):
    ret=setDBMS(dbms) if param=="" else setDBMS(dbms,param)
    r=ret[1][idx] if act else ret[0][idx]
    return r

def listSproc(dbms="sqlite"): return setDBMS(dbms)

def safeStr(m,x='encode'):#Util...change carefully
    import base64
    if x=='encode':r=base64.b64encode(str(m).encode('ascii')).decode('ascii')
    else:r=base64.b64decode(m.encode('ascii')).decode('ascii')
    return r    
if __name__ == "__main__":
    import sys
    a=sys.argv
    if len(a)<3:#"list"
        sql=listSproc()
        for i in sql[0]: print(i)
    elif len(a)==2:
        print(getSproc(a[1],a[2],"AND 2=2 "))
