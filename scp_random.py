#!/usr/bin/env python3
# POC: 
#Author: scp
#import random #always, always import random...
from turtle import Turtle,Screen
import random
import time
import subprocess

def mutantTurtle(t,rnd):
    t.spokes=random.randint(2,36) if rnd else 3
    t.dr=random.randint(15,100)
    t.i=random.randint(0,10)
    t.rot=360/t.spokes
    t.a=random.randint(1,120)
    t.cp=random.randint(0,4)
    print(t.name," :",t.spokes,t.rot,t.dr,t.i,t.a,rnd)

def rndC(): return random.randint(0,255)
def rndT(): return 50+random.randint(0,25)
def rcG(t): t.color((rndT(),rndC(),rndT()))
def rcR(t): t.color((rndC(),rndT(),rndT()))
def rcB(t): t.color((rndT(),rndT(),rndC()))
def rc(t): t.color((rndC(),rndC(),rndC()))

def moves(t,step,it,angle=30):
    t.forward(step)
    t.left(angle)
    if(it==0) and (step<30): d=6*step/7#else: d=8*step/9
    elif(it<5): d=4*step/5
    else: d=3*step/4
    draw(d,t.i)
    t.right(angle*2)
    draw(d,t.i)
    t.left(angle)
    t.backward(step)

def printFinal(f = './MyFrac'+time.strftime("%Y%m%d-%H%M%S")+'.ps'):
    print(f)
    canvas = wn.getcanvas()
    canvas.postscript(file=f)
    subprocess.call(['lpr', f])
    
if __name__ == "__main__":
    wn = Screen() #wn.setup(1200, 1024)
    wn.setup(startx=None, starty=None)
    wn.colormode(255)
    wn.bgcolor(0,0,0)# compliment 240.0.0.0/4 240.0.0.0?255.255.255.254 268435455 InternetReserved for future use.[13] (Former Class E network.)
      #wn.bgpic('bgimg1.png')
    wn.title("<======================Turtle walks Frac Step====================>")
    mainTurtle = Turtle()#(shape="turtle")
    mainTurtle.name="leo"
    wn.colormode(255)#wn.bgcolor("black") #Screen Bg color
    wn.bgcolor(0,0,0)
    startPos=mainTurtle.pos()
    mutantTurtle(mainTurtle,True)
    mainTurtle.left(90) #moving the turtle 90 degrees towards left
    mainTurtle.speed(20)#setting the speed of the turtle
    mainTurtle.pensize(2)
    if 1:#while 1:#wn.bye() #wn.exitonclick()
        #=======================
        for cnt in range(1): #printFinal()?
            for spoke in range(mainTurtle.spokes):
                def  draw(s,i):
                    if(s<10): return
                    else:
                        if (mainTurtle.cp==0):rc(mainTurtle)
                        elif(mainTurtle.cp==1):rcR(mainTurtle)
                        elif(mainTurtle.cp==2):rcB(mainTurtle)
                        else:rcG(mainTurtle)
                        moves(mainTurtle,s,i,mainTurtle.a)
                draw(mainTurtle.dr,mainTurtle.i)  
                mainTurtle.right(mainTurtle.rot)
            mainTurtle.right(random.randint(0,359))
            mainTurtle.penup()
            mainTurtle.goto(startPos)
            mutantTurtle(mainTurtle,True)
            mainTurtle.forward(random.randint(0,500))
            mainTurtle.pendown()

            
