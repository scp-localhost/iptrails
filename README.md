# ipTrails
<p align="center">
<a href="http://192.168.0.15/scp-localhost/iptrails">
<img src="images/logo.png" alt="Logo" width="1590" height="830">
</a>
<h3 align="center">ipTrails a honeyPig related Project</h3>
<p align="center">
Plotting IP Subnets by degrees of Separation
<br />
<a href="http://192.168.0.15/scp-localhost/iptrails"><strong>Explore the docs »</strong></a>
<br />
<br />
<a href="http://192.168.0.15/scp-localhost/iptrails">View Demo</a>
·
<a href="http://192.168.0.15/scp-localhost/iptrails/issues">Report Bug</a>
·
<a href="http://192.168.0.15/scp-localhost/iptrails/issues">Request Feature</a>
</p>
</p>
<!-- ToC -->
<details open="open">
<summary>Table of Contents</summary>
<ol>
<li>
<a href="#about-the-project">About iptrails...</a>
<ul>
<li><a href="#built-with">Built With</a></li>
</ul>
</li>
<li>
<a href="#getting-started">Getting Started</a>
<ul>
<li><a href="#prerequisites">Prerequisites</a></li>
<li><a href="#installation">Installation</a></li>
</ul>
</li>
<li><a href="#usage">Usage</a></li>
<li><a href="#roadmap">Roadmap</a></li>
<li><a href="#contributing">Contributing</a></li>
<li><a href="#license">License</a></li>
<li><a href="#contact">Contact</a></li>
<li><a href="#acknowledgements">Acknowledgements</a></li>
</ol>
</details>

<!-- ABOUT THE PROJECT -->
## About The Project

[![Product Name Screen Shot][product-screenshot]](http://192.168.0.15/scp-localhost/iptrails)

...This started as a text concatination of IP addresses from port scans with :pig: Snort rules syntax and lookup Urls for services like Shodan and Censys. Working on minimal hardware has always been a "Spec". This will run on Pi or Wifi Pineappple with minimal massaging.

The data source can be any IP4 address list. Default db included, ~ways to plot live. 
With python's scapy it will read pcaps or plot live sniffing.

Large plots will bog down even strong systems eventually. 
Using Turtle may embarass you in front of other Python nerds, show them the pictures.
Not everything looks cool, ask it an IP4 question like a magic 8 ball.

### Nmap of local subnet from pcap
<img src="images/nmap-sX_192_168_0_0_24.png" alt="home">

### High volume scanners Shodan and Censys by reverse IP lookup
<img src="images/shodan_censys_DNS.png" alt="scanners">

### <a href="https://weberblog.net/the-ultimate-pcap/">The Ultimate PCAP v20200224</a>
<img src="images/ultPcap.png" alt="Ult Pcap">

### RDP by packet dissection (not port)
<img src="images/rdp.png" alt="Rdp">


### Built With

* [Python](https://www.python.org/)


<!-- GETTING STARTED -->
## Getting Started

iptrails is meant to run with minimal effort and resources.
It is written mostly in Python3 and should run in ~Debian like environments without fuss.:smile:

### Caveats

* :pig: Hacking back is a bad idea. Honeypots are close. This is like walking into a rough bar with fake money hanging from your pocket...You'll get beaten up once by chance and again for being a wise-ass.
That said, the project requires that you open the ports you want to listen on to the outside whatever that may mean in your environment. (Port mapping at a router and device firewall rules minimally). Realize this is a safety feature you are disabling.

* :pig: Don't step on your own ports...It is possible to listen and host the web app on one interface. Make sure you don't have iptrailss listening on the web app interface and port (or any other service for that matter). It may be useful to redirect port traffic to a listener pig on a less common port to avoid a needed service (ike SSH or VNC).

* :pig: Hacking back is a bad idea. There are places where iptrails (and related warPig/honeyPig) can do aggresive things. ~rick rolling logs etc. You should confine this to your own network or to a scope where you have permission for your payload.

* :pig: The web app is meant for simple local reporting and is not robust enough for a public facing app. If you use it like that and something bad happens, it is your own fault. 


### Prerequisites

Python3 assumed?

* sqlite3
```sh
sudo apt-get install sqlite3
```
* missing module? just try this...
```sh
sudo python3 -m pip install [whatever]
```
* scapy to consume pcaps. OS mileage may vary. DB and list data sources still work

### Installation

1. Python, imports... 
2. Extras; pcaps, IP lists, data sources.
3. Clone the repo
```sh
git clone http://192.168.0.15/scp-localhost/iptrails.git
cd iptrails
```
4. Most vars are pretty obvious, adjust your data visualization, query and list filters
5. Take a break. This is not fast. 


<!-- USAGE EXAMPLES -->
## Usage

### Basic - connect to default db, plot all IP's in default query, do not make ps output.
```sh
python3 honeyFrac.py
```
### Params...maybe soon
```sh
python3 honeyFrac.py [ip list source, var list]
```
### Defaults and Config
1. DB and pcap are path refernces defaulting to ./data. Samples given. DB model is common to honeyPig (interchangable DB's)
Plot is sequential, duplicates redraw.
2. Big" data sets drop the inner most spoke - the Class A Octet to allow more visibilitiy of destinations inside the spoke's radius.
3. Color is by Class A,B and C. The backgound is a ~hot black, not 255,255,255 because 255.255.255.255 could have a line depending on the pcap or scan.


 	
	
	<!-- ROADMAP -->
	## Roadmap
	
	See :pig: the [open issues](http://192.168.0.15/scp-localhost/iptrails/issues) for a list of proposed features (and known issues). Eventually...
	
	<!-- CONTRIBUTING -->
	## Contributing
	
	Contributions are what make the open source community such an amazing place to be learn, inspire, and create. Any contributions you make are **greatly appreciated**.
	
	1. Fork the Project
	2. Create your Feature Branch (`git checkout -b feature/AmazingFeature`)
	3. Commit your Changes (`git commit -m 'Add some AmazingFeature'`)
	4. Push to the Branch (`git push origin feature/AmazingFeature`)
	5. Open a Pull Request
	
	<!-- LICENSE -->
	## License
	Distributed under the MIT License. See `LICENSE` for more information.
	
	<!-- CONTACT -->
	## Contact
	
	scp - [@scp15487477](https://twitter.com/scp15487477) - steve.pote@protonmail.com :japanese_ogre:
	
	[![LinkedIn][linkedin-shield]][linkedin-url]
	
	Project Link: [http://192.168.0.15/scp-localhost/iptrails](http://192.168.0.15/scp-localhost/iptrails)
	
	
	<!-- ACKNOWLEDGEMENTS -->
	## Acknowledgements
	* [??](https://www.google.com/)
	* [Pigs](https://www.google.com/search?q=pigs)
	* [Snort](https://www.snort.org/)
	
	<!-- MARKDOWN LINKS & IMAGES -->
	<!-- https://img.shields.io/static/v1?label=<LABEL>&message=<MESSAGE>&color=<COLOR> -->
	<!-- https://www.markdownguide.org/basic-syntax/#reference-style-links -->
	[linkedin-shield]: https://img.shields.io/badge/-LinkedIn-black.svg?style=for-the-badge&logo=linkedin&colorB=555
	[linkedin-url]: https://www.linkedin.com/in/steve-pote/
	[product-screenshot]: images/screenshot.png
